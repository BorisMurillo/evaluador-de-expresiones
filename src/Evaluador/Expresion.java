/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluador;

import java.util.ListIterator;
import ufps.util.colecciones_seed.*;
import ufps.util.colecciones_seed.Cola;

/**
 *
 * @author estudiante
 */
public class Expresion {

    private ListaCD<String> expresiones = new ListaCD();

    public Expresion() {
    }

    public Expresion(String cadena) {

        String v[] = cadena.split(",");
        for (String dato : v) {
            this.expresiones.insertarAlFinal(dato);
        }
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
        String msg = "";
        for (String dato : this.expresiones) {
            msg += dato + "<->";
        }
        return msg;
    }

    public String getPrefijo() {

        ListIterator<String> itInverso = this.expresiones.iteratorInverso();
        Pila<String> pila = new Pila<>();
        Pila<String> pilaAux = new Pila();

        if (expresiones.esVacia() || expresiones.getTamanio() <= 2) {
            return "";
        }

        while (itInverso.hasPrevious()) {
//        for(NodoD<String> t=expresiones.getCabeza().getAnt();t!=expresiones.getCabeza();t=t.getAnt()){
            String t = itInverso.previous();

            revisarDesapilar(pila, pilaAux);

            if (prioridad(t) != -1)//si lo que hay en lista es un signo
            {
                if (pilaAux.esVacia()) {
                    pilaAux.apilar(t);
                } else if (prioridad(pilaAux.getTope()) <= prioridad(t)) {//si el signo que hay en pilaAux es prioritario a lo que hay en lista

                    if (pilaAux.getTope().equals(")")) {
                        pilaAux.apilar(t);
                    } else {
                        pila.apilar(pilaAux.desapilar());
                        if (!pilaAux.esVacia() && prioridad(t) > prioridad(pilaAux.getTope()))//si el signo que esta en la pilaAux es prioritario al signo de la lista 
                        {
                            pila.apilar(pilaAux.desapilar());
                        }
                        pilaAux.apilar(t);
                    }
                } else {
                    pilaAux.apilar(t);
                }

            } else {
                pila.apilar(t);
            }

        }

        if (!pilaAux.esVacia()) {
            vaciar2(pila, pilaAux);
        }

        return pila.toString();
    }

    private void revisarDesapilar(Pila<String> pila, Pila<String> pilaAux) {

        if (!pilaAux.esVacia() && pilaAux.getTope().equals("(")) {

            pilaAux.desapilar();

            while (!pilaAux.getTope().equals(")")) {

                pila.apilar(pilaAux.desapilar());

            }
            pilaAux.desapilar();
        }

    }

    private void vaciar2(Pila<String> pila, Pila<String> pila2) {

        while (!pila2.esVacia()) {
            if (pila2.getTope().equals("(") || pila2.getTope().equals(")")) {
                pila2.desapilar();
            } else {
                pila.apilar(pila2.desapilar());
            }
        }

    }

    public String getPosfijo() {

        if (expresiones.esVacia() || expresiones.getTamanio() == 2) {
            return "";
        }

        Pila<String> pila = new Pila<>();
        Cola<String> cola = new Cola<>();

        for (String t : this.expresiones) {

            desapilarParentesis(cola, pila);

            if (prioridad(t) != -1) {
                if (pila.esVacia()) {
                    pila.apilar(t);
                } else if (prioridad(t) >= prioridad(pila.getTope())) {
          
                        if(pila.getTope().equals("("))
                            pila.apilar(t);
                        else{    
                            
                        cola.enColar(pila.desapilar());
                        if (!pila.esVacia() && prioridad(t) > prioridad(pila.getTope()))//si el signo que esta en la pilaAux es prioritario al signo de la lista 
                        {
                            cola.enColar(pila.desapilar());
                        }
                        pila.apilar(t);
                        
                        }
                    
                } else {               
                    pila.apilar(t);
                }
            } else {
                cola.enColar(t);              
            }

        }
        
        if (!pila.esVacia()) {
            vaciar(pila, cola);
        }

        return cola.toString();
    }

    private void desapilarParentesis(Cola<String> cola, Pila<String> pila) {
        
        if (!pila.esVacia() && pila.getTope().equals(")")) {
          
            pila.desapilar();
            while (!pila.getTope().equals("(")) {
                
                cola.enColar(pila.desapilar());
           
            }
            pila.desapilar();
        }
    }

    private void vaciar(Pila<String> pila, Cola<String> cola) {
        while (!pila.esVacia()) {
            if (pila.getTope().equals("(") || pila.getTope().equals(")")) {
                pila.desapilar();
            } else {
                cola.enColar(pila.desapilar());
            }
        }

    }

    private int prioridad(String x) {

        if (x.equals("(")) {
            return 0;
        }

        if (x.equals(")")) {
            return 1;
        }

        if (x.equals("/")) {
            return 2;
        }

        if (x.equals("*")) {
            return 3;
        }

        if (x.equals("+") || x.equals("-")) {
            return 4;
        }

        return -1;
    }

    public float getEvaluarPosfijo(String postfijo) {
        String[] v = postfijo.split("->");
        Pila<String> pilaNum = new Pila<>();
        Pila<String> pilaSig = new Pila<>();
        for (String dato : v) {
            if (prioridad(dato + "") == -1) {
                pilaNum.apilar(dato + "");
            } else {
                pilaSig.apilar(dato + "");
                if (pilaSig.getTamanio() == 1) {
                    resultado(pilaNum, pilaSig.desapilar());
                }
            }

        }

        return Float.parseFloat(pilaNum.desapilar());
    }

    private void resultado(Pila<String> pilaNum, String signo) {
        if (pilaNum.getTamanio() >= 2) {
            float op1 = Float.parseFloat(pilaNum.desapilar());
            float op2 = Float.parseFloat(pilaNum.desapilar());
            String resultado = "";
            Float res;
            switch (signo) {

                case "*":
                    res = op1 * op2;
                    resultado = res.toString();
                    pilaNum.apilar(resultado);
                    break;

                case "/":
                    res = op1 / op2;
                    resultado = res.toString();
                    pilaNum.apilar(resultado);
                    break;
                case "+":
                    res = op1 + op2;
                    resultado = res.toString();
                    pilaNum.apilar(resultado);
                    break;
                case "-":
                    res = op1 - op2;
                    resultado = res.toString();
                    pilaNum.apilar(resultado);
                    break;
            }
        }

    }
}