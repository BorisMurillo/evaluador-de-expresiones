/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import java.util.ListIterator;
import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class TestInverso {
    
    
    public static void main(String[] args) {
        ListaCD<String> l=new ListaCD<String>();
        l.insertarAlFinal("marco");
        l.insertarAlFinal("peña");
        l.insertarAlFinal("boris");
        for(String dato: l)
                System.out.print(dato+"\t");
       System.out.print("\n");
       
       ListIterator<String> itInverso=l.iteratorInverso();
       while (itInverso.hasPrevious())
                System.out.print(itInverso.previous()+"\t");
        
        
    }
    
}
